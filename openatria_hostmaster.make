core = 6.x
api = 2


;includes[hostmaster] = "http://drupalcode.org/project/hostmaster.git/blob_plain/refs/heads/6.x-2.x:/hostmaster.make"

projects[hosting][type] = "module"
projects[hosting][download][type] = "git"
projects[hosting][download][url] = "http://git.drupal.org/sandbox/ergonlogic/1468048.git"
projects[hosting][download][branch] = "master"

;libraries[profiler][download][type] = "get"
;libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-6.x-2.0-beta2.tar.gz"
